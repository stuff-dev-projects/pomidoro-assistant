from os import path

from utils.logging_info import LoggingInfo

ROOT_DIR = path.dirname(path.abspath(__file__))

logger = LoggingInfo(service="Pomidoro Assistant", log_level="debug")
logger.log_to_file(file_name=f"{ROOT_DIR}/logs/POM_ASS_LOG")
