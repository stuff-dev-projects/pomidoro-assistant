from PyQt5.QtCore import QTime, QTimer
from datetime import time


def digital_time_setup(lcd_number, work_minutes):
    lcd_number.title = "Digital Time"
    lcd_number.setDecMode()
    lcd_number.setSegmentStyle(lcd_number.Filled)

    # Getting value from work_time_edit and displaying to lcd number
    work_time = work_minutes.dateTime().toString('mm:ss')
    lcd_time = QTime.fromString(f"{work_time}", "m:s")
    text = lcd_time.toString('mm:ss')
    text = text[:2] + ':' + text[3:]
    lcd_number.display(text)
