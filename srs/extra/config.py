from datetime import time

from PyQt5 import QtCore, QtWidgets


def config_widget_setup(widget):
    widget.setHidden(True)
    widget.setObjectName("config_widget")
    widget.resize(418, 340)
    widget.setStyleSheet("background-color: rgb(46, 52, 54);")

    # Configuration time group box
    widget.time_group_box = QtWidgets.QGroupBox(widget)
    widget.time_group_box.setGeometry(QtCore.QRect(193, 5, 218, 140))
    widget.time_group_box.setObjectName("time_group_box")
    widget.time_widget = QtWidgets.QWidget(widget.time_group_box)
    widget.time_widget.setGeometry(QtCore.QRect(20, 30, 173, 94))
    widget.time_widget.setObjectName("widget")

    # Time horizontal layout
    widget.time_horizontal_layout = QtWidgets.QHBoxLayout(widget.time_widget)
    widget.time_horizontal_layout.setContentsMargins(0, 0, 0, 0)
    widget.time_horizontal_layout.setObjectName("time_horizontal_layout")

    # Label vertical layout
    widget.label_vertical_layout = QtWidgets.QVBoxLayout()
    widget.label_vertical_layout.setObjectName("label_vertical_layout")
    widget.small_break_label = QtWidgets.QLabel(widget.time_widget)
    widget.small_break_label.setObjectName("small_break_label")
    widget.label_vertical_layout.addWidget(widget.small_break_label)
    widget.big_break_label = QtWidgets.QLabel(widget.time_widget)
    widget.big_break_label.setObjectName("big_break_label")
    widget.label_vertical_layout.addWidget(widget.big_break_label)
    widget.work_time_label = QtWidgets.QLabel(widget.time_widget)
    widget.work_time_label.setObjectName("work_time_label")
    widget.label_vertical_layout.addWidget(widget.work_time_label)
    widget.time_horizontal_layout.addLayout(widget.label_vertical_layout)

    # Time edit vertical layout
    widget.time_edit_vertical_layout = QtWidgets.QVBoxLayout()
    widget.time_edit_vertical_layout.setObjectName("time_edit_vertical_layout")
    widget.small_break_time_edit = QtWidgets.QTimeEdit(widget.time_widget)
    widget.small_break_time_edit.setEnabled(True)
    sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(widget.small_break_time_edit.sizePolicy().hasHeightForWidth())
    widget.small_break_time_edit.setSizePolicy(sizePolicy)
    widget.small_break_time_edit.setLayoutDirection(QtCore.Qt.LeftToRight)
    widget.small_break_time_edit.setCurrentSection(QtWidgets.QDateTimeEdit.MinuteSection)
    widget.small_break_time_edit.setObjectName("small_break_time_edit")
    widget.time_edit_vertical_layout.addWidget(widget.small_break_time_edit)
    widget.big_break_time_edit = QtWidgets.QTimeEdit(widget.time_widget)
    widget.big_break_time_edit.setObjectName("big_break_time_edit")
    widget.time_edit_vertical_layout.addWidget(widget.big_break_time_edit)
    widget.work_time_edit = QtWidgets.QTimeEdit(widget.time_widget)
    widget.work_time_edit.setObjectName("work_time_edit")
    widget.time_edit_vertical_layout.addWidget(widget.work_time_edit)
    widget.time_horizontal_layout.addLayout(widget.time_edit_vertical_layout)

    # Min label vertical layout
    widget.min_label_vertical_layout = QtWidgets.QVBoxLayout()
    widget.min_label_vertical_layout.setObjectName("min_label_vertical_layout")
    widget.min_label_1 = QtWidgets.QLabel(widget.time_widget)
    widget.min_label_1.setObjectName("min_label_1")
    widget.min_label_vertical_layout.addWidget(widget.min_label_1)
    widget.min_label_2 = QtWidgets.QLabel(widget.time_widget)
    widget.min_label_2.setObjectName("min_label_2")
    widget.min_label_vertical_layout.addWidget(widget.min_label_2)
    widget.min_label_3 = QtWidgets.QLabel(widget.time_widget)
    widget.min_label_3.setObjectName("min_label_3")
    widget.min_label_vertical_layout.addWidget(widget.min_label_3)
    widget.time_horizontal_layout.addLayout(widget.min_label_vertical_layout)

    # Configuration pomidoro group box
    widget.pomidoro_group_box = QtWidgets.QGroupBox(widget)
    widget.pomidoro_group_box.setGeometry(QtCore.QRect(5, 5, 185, 140))
    widget.pomidoro_group_box.setObjectName("pomidoro_group_box")
    widget.pomidoro_label = QtWidgets.QLabel(widget.pomidoro_group_box)
    widget.pomidoro_label.setGeometry(QtCore.QRect(10, 30, 91, 17))
    widget.pomidoro_label.setObjectName("pomidoro_label")
    widget.pomidoro_widget = QtWidgets.QWidget(widget.pomidoro_group_box)
    widget.pomidoro_widget.setGeometry(QtCore.QRect(30, 50, 124, 62))
    widget.pomidoro_widget.setObjectName("pomidoro_widget")

    # Pomidoro vertical layout
    widget.pomidoro_vertical_layout = QtWidgets.QVBoxLayout(widget.pomidoro_widget)
    widget.pomidoro_vertical_layout.setContentsMargins(0, 0, 0, 0)
    widget.pomidoro_vertical_layout.setObjectName("pomidoro_vertical_layout")

    # Round horizontal layout
    widget.round_horizontal_layout = QtWidgets.QHBoxLayout()
    widget.round_horizontal_layout.setObjectName("round_horizontal_layout")
    widget.round_label = QtWidgets.QLabel(widget.pomidoro_widget)
    widget.round_label.setObjectName("round_label")
    widget.round_horizontal_layout.addWidget(widget.round_label)
    widget.round_spin_box = QtWidgets.QSpinBox(widget.pomidoro_widget)
    widget.round_spin_box.setObjectName("round_spin_box")
    widget.round_horizontal_layout.addWidget(widget.round_spin_box)
    widget.pomidoro_vertical_layout.addLayout(widget.round_horizontal_layout)

    # Day horizontal layout
    widget.day_horizontal_layout = QtWidgets.QHBoxLayout()
    widget.day_horizontal_layout.setObjectName("day_horizontal_layout")
    widget.day_label = QtWidgets.QLabel(widget.pomidoro_widget)
    widget.day_label.setObjectName("day_label")
    widget.day_horizontal_layout.addWidget(widget.day_label)
    widget.day_spin_box = QtWidgets.QSpinBox(widget.pomidoro_widget)
    widget.day_spin_box.setObjectName("day_spin_box")
    widget.day_horizontal_layout.addWidget(widget.day_spin_box)
    widget.pomidoro_vertical_layout.addLayout(widget.day_horizontal_layout)

    # Configuration notification group box
    widget.notification_group_box = QtWidgets.QGroupBox(widget)
    widget.notification_group_box.setGeometry(QtCore.QRect(193, 150, 218, 111))
    widget.notification_group_box.setObjectName("notification_group_box")

    widget.notification_label = QtWidgets.QLabel(widget.notification_group_box)
    widget.notification_label.setGeometry(QtCore.QRect(10, 30, 111, 17))
    widget.notification_label.setObjectName("notification_label")

    widget.notification_widget = QtWidgets.QWidget(widget.notification_group_box)
    widget.notification_widget.setGeometry(QtCore.QRect(30, 50, 69, 54))
    widget.notification_widget.setObjectName("notification_widget")

    # Notification vertical layout
    widget.notification_vertical_layout = QtWidgets.QVBoxLayout(widget.notification_widget)
    widget.notification_vertical_layout.setContentsMargins(0, 0, 0, 0)
    widget.notification_vertical_layout.setObjectName("notification_vertical_layout")

    widget.text_check_box = QtWidgets.QCheckBox(widget.notification_widget)
    widget.text_check_box.setLayoutDirection(QtCore.Qt.LeftToRight)
    widget.text_check_box.setObjectName("text_check_box")
    widget.notification_vertical_layout.addWidget(widget.text_check_box)

    widget.sound_check_box = QtWidgets.QCheckBox(widget.notification_widget)
    widget.sound_check_box.setLayoutDirection(QtCore.Qt.LeftToRight)
    widget.sound_check_box.setObjectName("sound_check_box")
    widget.notification_vertical_layout.addWidget(widget.sound_check_box)

    # Configuration basic group box
    widget.basic_group_box = QtWidgets.QGroupBox(widget)
    widget.basic_group_box.setGeometry(QtCore.QRect(5, 150, 185, 140))
    widget.basic_group_box.setObjectName("basic_group_box")
    widget.basic_widget = QtWidgets.QWidget(widget.basic_group_box)
    widget.basic_widget.setGeometry(QtCore.QRect(10, 30, 161, 27))
    widget.basic_widget.setObjectName("basic_widget")

    # Theme horizontal layout
    widget.theme_horizontal_layout = QtWidgets.QHBoxLayout(widget.basic_widget)
    widget.theme_horizontal_layout.setContentsMargins(0, 0, 0, 0)
    widget.theme_horizontal_layout.setObjectName("theme_horizontal_layout")
    widget.theme_label = QtWidgets.QLabel(widget.basic_widget)
    widget.theme_label.setObjectName("theme_label")
    widget.theme_horizontal_layout.addWidget(widget.theme_label)
    widget.theme_combo_box = QtWidgets.QComboBox(widget.basic_widget)
    widget.theme_combo_box.setObjectName("theme_combo_box")
    widget.theme_horizontal_layout.addWidget(widget.theme_combo_box)

    # Button box
    widget.config_button_box = QtWidgets.QDialogButtonBox(widget)
    widget.config_button_box.setGeometry(QtCore.QRect(193, 270, 221, 25))
    widget.config_button_box.setStandardButtons(
        QtWidgets.QDialogButtonBox.Apply |
        QtWidgets.QDialogButtonBox.Close |
        QtWidgets.QDialogButtonBox.Reset
    )
    widget.config_button_box.setObjectName("config_button_box")

    # Last step of configuration
    config_widget_re_translate_ui(widget)
    set_config_default_settings(widget)
    QtCore.QMetaObject.connectSlotsByName(widget)


def config_widget_re_translate_ui(widget):
    _translate = QtCore.QCoreApplication.translate
    widget.setWindowTitle(_translate("config_window", "Config Dialog"))

    # Group box
    widget.time_group_box.setTitle(_translate("config_window", "Time settings"))
    widget.pomidoro_group_box.setTitle(_translate("config_window", "Pomidoro settings"))
    widget.basic_group_box.setTitle(_translate("config_window", "Basic settings"))
    widget.notification_group_box.setTitle(_translate("config_window", "Notifications settings"))

    # Label
    widget.small_break_label.setText(_translate("config_window", "Small break"))
    widget.big_break_label.setText(_translate("config_window", "Big break"))
    widget.work_time_label.setText(_translate("config_window", "Work time"))
    widget.pomidoro_label.setText(_translate("config_window", "Tomatoes ..."))
    widget.round_label.setText(_translate("config_window", "in a round"))
    widget.day_label.setText(_translate("config_window", "in the day "))
    widget.theme_label.setText(_translate("config_window", "Theme color"))
    widget.notification_label.setText(_translate("config_window", "Notification ..."))
    widget.min_label_1.setText(_translate("config_window", "min"))
    widget.min_label_2.setText(_translate("config_window", "min"))
    widget.min_label_3.setText(_translate("config_window", "min"))

    # Check box
    widget.text_check_box.setText(_translate("config_window", "text"))
    widget.sound_check_box.setText(_translate("config_window", "sound"))
    widget.text_check_box.setChecked(False)
    widget.sound_check_box.setChecked(False)

    # Spin box
    widget.round_spin_box.setMinimum(1)
    widget.day_spin_box.setMinimum(1)

    # Combo box
    widget.theme_combo_box.addItems([
        " Dark ",
        " Light "
    ])

    # Time edit
    widget.small_break_time_edit.setDisplayFormat("mm")
    widget.big_break_time_edit.setDisplayFormat("mm")
    widget.work_time_edit.setDisplayFormat("mm")

    widget.small_break_time_edit.setMinimumTime(time(minute=1))
    widget.big_break_time_edit.setMinimumTime(time(minute=1))
    widget.work_time_edit.setMinimumTime(time(minute=1))


def set_config_default_settings(widget):
    # 'Time' default settings
    small_break_default_time = time(minute=5)
    big_break_default_time = time(minute=20)
    work_time_default_time = time(minute=40)

    widget.small_break_time_edit.setTime(small_break_default_time)
    widget.big_break_time_edit.setTime(big_break_default_time)
    widget.work_time_edit.setTime(work_time_default_time)

    # 'Pomidoro' default settings
    widget.round_spin_box.setValue(4)
    widget.day_spin_box.setValue(12)

    # 'Notification' default settings
    widget.text_check_box.setChecked(False)
    widget.sound_check_box.setChecked(False)

    # 'Basic' default settings
    widget.theme_combo_box.setCurrentIndex(0)
