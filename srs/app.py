from datetime import time

from PyQt5 import QtCore, QtGui, QtWidgets

from settings import logger
from srs.about import AboutDialogUi
from srs.confirm import ConfirmDialogUi
from srs.extra.config import config_widget_setup, set_config_default_settings
from srs.extra.digital_time import digital_time_setup


class AppWindowUi(QtWidgets.QMainWindow):
    def __init__(self):
        super(AppWindowUi, self).__init__()

        self.app = QtWidgets.QMainWindow()
        self.central_widget = QtWidgets.QWidget(self.app)
        self.start_button = QtWidgets.QPushButton(self.central_widget)
        self.stop_button = QtWidgets.QPushButton(self.central_widget)
        self.lcd_number = QtWidgets.QLCDNumber(self.central_widget)
        self.work_break_label = QtWidgets.QLabel(self.central_widget)
        self.menu_bar = QtWidgets.QMenuBar(self.app)
        self.menu_settings = QtWidgets.QMenu(self.menu_bar)
        self.menu_more = QtWidgets.QMenu(self.menu_bar)
        self.status_bar = QtWidgets.QStatusBar(self.app)
        self.action_about = QtWidgets.QAction(self.app)
        self.action_config = QtWidgets.QAction(self.app)
        self.action_quit = QtWidgets.QAction(self.app)
        self.config_widget = QtWidgets.QWidget(self.central_widget)

        self.timer = QtCore.QTimer(self.lcd_number)
        self.lcd_work_status = False

        self.app.setFixedSize(418, 340)
        self.setup_ui(self.app)
        self.app.show()

        logger.info("Program: 'Pomidoro Assistant' start ...")

    def setup_ui(self, app_window):
        """Setup window parameters."""
        app_window.setObjectName("app_window")
        app_window.setFocusPolicy(QtCore.Qt.WheelFocus)
        app_window.setAutoFillBackground(False)
        app_window.setStyleSheet("background-color: rgb(46, 52, 54);\n""")
        app_window.setAnimated(True)

        self.central_widget.setObjectName("central_widget")
        app_window.setCentralWidget(self.central_widget)

        # Start button
        self.start_button.setGeometry(QtCore.QRect(220, 230, 90, 50))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.start_button.setFont(font)
        self.start_button.setObjectName("start_button")
        self.start_button.clicked.connect(self.start_button_pressed)

        # Stop button
        self.stop_button.setGeometry(QtCore.QRect(320, 230, 90, 50))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.stop_button.setFont(font)
        self.stop_button.setObjectName("stop_button")
        self.stop_button.clicked.connect(self.stop_button_pressed)

        # LCD number
        self.lcd_number.setGeometry(QtCore.QRect(10, 0, 398, 220))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lcd_number.sizePolicy().hasHeightForWidth())
        self.lcd_number.setSizePolicy(sizePolicy)
        self.lcd_number.setObjectName("lcd_number")

        # Work_break label
        self.work_break_label.setGeometry(QtCore.QRect(20, 230, 191, 50))
        self.work_break_label.setMinimumSize(QtCore.QSize(0, 0))
        font = QtGui.QFont()
        font.setPointSize(25)
        self.work_break_label.setFont(font)
        self.work_break_label.setObjectName("work_break_label")
        self.work_break_label.setStyleSheet("color: rgb(164, 0, 0);")
        self.work_break_label.setVisible(False)

        # Menu bar
        self.menu_bar.setGeometry(QtCore.QRect(0, 0, 355, 22))
        self.menu_bar.setObjectName("menu_bar")
        self.menu_settings.setObjectName("menu_settings")
        self.menu_more.setObjectName("menu_more")
        app_window.setMenuBar(self.menu_bar)

        self.status_bar.setObjectName("status_bar")
        app_window.setStatusBar(self.status_bar)

        self.action_about.setObjectName("action_about")
        self.action_about.triggered.connect(self.menu_bar_more_about_pressed)

        self.action_config.setObjectName("action_config")
        self.action_config.triggered.connect(self.menu_bar_settings_configuration_pressed)

        self.action_quit.setObjectName("action_quit")
        self.action_quit.triggered.connect(self.menu_bar_settings_quit_pressed)

        self.menu_settings.addAction(self.action_config)
        self.menu_settings.addAction(self.action_quit)
        self.menu_more.addAction(self.action_about)
        self.menu_bar.addAction(self.menu_settings.menuAction())
        self.menu_bar.addAction(self.menu_more.menuAction())

        # Config setup
        config_widget_setup(widget=self.config_widget)
        self.config_widget.config_button_box.clicked.connect(self.config_standard_buttons_clicked)

        # LCD setup
        digital_time_setup(lcd_number=self.lcd_number, work_minutes=self.config_widget.work_time_edit)

        # Last setup step
        self.re_translate_ui(app_window)
        QtCore.QMetaObject.connectSlotsByName(app_window)

    def re_translate_ui(self, app_window):
        _translate = QtCore.QCoreApplication.translate
        app_window.setWindowTitle(_translate("app_window", "Pomidoro Assistant"))
        self.start_button.setText(_translate("app_window", "Start"))
        self.stop_button.setText(_translate("app_window", "Stop"))
        self.menu_settings.setTitle(_translate("app_window", "Settings"))
        self.menu_more.setTitle(_translate("app_window", "More"))
        self.action_about.setText(_translate("app_window", "About"))
        self.action_config.setText(_translate("app_window", "Configuration"))
        self.action_quit.setText(_translate("app_window", "Quit"))
        self.work_break_label.setText(_translate("app_window", "Work time"))

    # Configuration lcd number
    def config_lcd(self):
        """Configuration LCD."""
        # self.timer = QtCore.QTimer(self.lcd_number)
        if self.lcd_work_status:
            self.timer.timeout.connect(self.show_time)
            self.timer.start(1000)
        # self.show_time()

    def show_time(self):
        """Show time to LCD."""
        # work_time_default_time = time(minute=33, second=21)
        # work_time = self.config_widget.work_time_edit.time()
        # str_work_time = work_time.toString('mm:ss')
        # lcd_time = QtCore.QTime.fromString(f"{str_work_time}", "m:s")

        work_time = self.config_widget.work_time_edit.time()
        text = work_time.toString('mm:ss')
        # if (work_time.second() % 2) == 0:
        #     text = text[:2] + ' ' + text[3:]
        # self.lcd_number.display(text)

        for s in range(10):
            wt = work_time.second()
            w = wt + 1
            if (w % 2) != 60:
                text = text[:2] + ' ' + text[3:]
            self.lcd_number.display(text)


    # Menu bar
    @staticmethod
    def menu_bar_more_about_pressed():
        """About button clicked in menu of main window."""
        logger.info("'About' in menu 'More' from main window was pressed.")
        try:
            about_dialog = AboutDialogUi()
            if about_dialog:
                logger.info("'About dialog' was closed.")
        except Warning:
            logger.warning("'About dialog' crashed.")

    def menu_bar_settings_configuration_pressed(self):
        """Configuration button clicked in menu of main window."""
        logger.info("'Configuration' in menu 'Settings' from main window was pressed.")
        self.config_widget.setHidden(False)

    def menu_bar_settings_quit_pressed(self):
        """Quit button clicked in menu of main window."""
        logger.info("'Quit' in menu 'Settings' from main window was pressed.")
        self.app.close()

    def start_button_pressed(self):
        """Start button clicked in main window."""
        logger.info("'Start' button from main window was pressed.")
        self.lcd_work_status = True
        self.lcd_number.setStyleSheet("background-color: rgb(255,51,51);\n""")
        self.work_break_label.setVisible(True)
        # self.lcd_number.setStyleSheet("background-color: rgb(51,51,255);\n""")
        self.config_lcd()

    def stop_button_pressed(self):
        """Stop button clicked in main window."""
        logger.info("'Stop' button from main window was pressed.")
        self.lcd_work_status = False
        self.timer.stop()
        self.lcd_number.setStyleSheet("background-color: rgb(46, 52, 54);\n""")
        self.work_break_label.setVisible(False)
        digital_time_setup(lcd_number=self.lcd_number, work_minutes=self.config_widget.work_time_edit)

    # Config button box
    def config_reset_button_pressed(self):
        """Reset button clicked in configuration window."""
        logger.info("'Reset' button from config window was pressed.")
        set_config_default_settings(self.config_widget)

    def config_apply_button_pressed(self):
        """Apply button clicked in configuration window."""
        logger.info("'Apply' button from config window was pressed.")
        try:
            confirm_dialog = ConfirmDialogUi(self.config_widget)
            if confirm_dialog:
                logger.info("'Confirm dialog' was closed.")
        except Warning:
            logger.warning("'Confirm dialog' crashed.")
        finally:
            digital_time_setup(lcd_number=self.lcd_number, work_minutes=self.config_widget.work_time_edit)
            theme_color = self.config_widget.theme_combo_box.currentIndex()
            if theme_color == 0:
                # about_dialog = AboutDialogUi()
                # confirm_dialog = ConfirmDialogUi(self.config_widget)
                # about_dialog.setStyleSheet("background-color: rgb(46, 52, 54);\n""")
                # confirm_dialog.setStyleSheet("background-color: rgb(46, 52, 54);\n""")
                self.app.setStyleSheet("background-color: rgb(46, 52, 54);\n""")
                self.config_widget.setStyleSheet("background-color: rgb(46, 52, 54);\n""")
            elif theme_color == 1:
                # about_dialog = AboutDialogUi()
                # confirm_dialog = ConfirmDialogUi(self.config_widget)
                # about_dialog.setStyleSheet("background-color: rgb(255, 255, 255);\n""")
                # confirm_dialog.setStyleSheet("background-color: rgb(255, 255, 255);\n""")
                self.app.setStyleSheet("background-color: rgb(255, 255, 255);\n""")
                self.config_widget.setStyleSheet("background-color: rgb(255, 255, 255);\n""")

    def config_close_button_pressed(self):
        """Close button clicked in configuration window."""
        logger.info("'Close' button from config window was pressed.")
        self.config_widget.setHidden(True)
        set_config_default_settings(self.config_widget)

    def config_standard_buttons_clicked(self, button):
        """
        Checks which button was clicked.
        :param button: Button that was clicked.
            :Apply: Saves current values.
            :Reset: Reset all values to default.
            :Close: Close configuration window.
        """
        button_box = self.config_widget.config_button_box
        if button_box.standardButton(button) == button_box.Reset:
            self.config_reset_button_pressed()
        elif button_box.standardButton(button) == button_box.Apply:
            self.config_apply_button_pressed()
        elif button_box.standardButton(button) == button_box.Close:
            self.config_close_button_pressed()
