from datetime import time

from PyQt5 import QtCore, QtWidgets

from settings import logger


class ConfirmDialogUi(QtWidgets.QDialog):
    def __init__(self, config_widget):
        super(ConfirmDialogUi, self).__init__()
        self.config_widget = config_widget
        self.dialog = QtWidgets.QDialog()
        self.confirm_box = QtWidgets.QDialogButtonBox(self.dialog)
        self.main_label = QtWidgets.QLabel(self.dialog)

        self.dialog.setFixedSize(330, 100)

        self.setup_ui(self.dialog)
        self.dialog.setModal(True)
        self.dialog.exec()

    def setup_ui(self, dialog):
        dialog.setObjectName("confirm_dialog")
        dialog.setStyleSheet("background-color: rgb(46, 52, 54);")

        self.confirm_box.setGeometry(QtCore.QRect(15, 60, 310, 30))
        self.confirm_box.setStyleSheet("")
        self.confirm_box.setOrientation(QtCore.Qt.Horizontal)
        self.confirm_box.setStandardButtons(QtWidgets.QDialogButtonBox.No | QtWidgets.QDialogButtonBox.Yes)
        self.confirm_box.setCenterButtons(True)
        self.confirm_box.setObjectName("confirm_box")
        self.confirm_box.accepted.connect(self.button_yes_pressed)
        self.confirm_box.rejected.connect(self.button_no_pressed)

        self.main_label.setGeometry(QtCore.QRect(0, 10, 311, 31))
        self.main_label.setSizeIncrement(QtCore.QSize(0, 10))
        self.main_label.setTextFormat(QtCore.Qt.PlainText)
        self.main_label.setAlignment(QtCore.Qt.AlignCenter)
        self.main_label.setObjectName("main_label")

        self.re_translate_ui(dialog)
        QtCore.QMetaObject.connectSlotsByName(dialog)

    def re_translate_ui(self, dialog):
        _translate = QtCore.QCoreApplication.translate
        dialog.setWindowTitle(_translate("confirm_dialog", "Confirm Dialog"))
        self.main_label.setText(_translate("confirm_dialog", "Are you sure you wanna apply changes ?"))

    def button_yes_pressed(self):
        logger.info("'Yes' button in 'Confirm dialog' was pressed")
        self.apply_new_config_settings()
        self.dialog.close()
        self.config_widget.setHidden(True)

    def button_no_pressed(self):
        logger.info("'No' button in 'Confirm dialog' was pressed")
        self.dialog.close()

    def apply_new_config_settings(self):
        # New 'Time' settings
        new_small_break_time = int(self.config_widget.small_break_time_edit.dateTime().toString('mm'))
        new_big_break_time = int(self.config_widget.big_break_time_edit.dateTime().toString('mm'))
        new_work_time = int(self.config_widget.work_time_edit.dateTime().toString('mm'))

        small_break_new_time = time(minute=new_small_break_time)
        big_break_new_time = time(minute=new_big_break_time)
        work_time_new_time = time(minute=new_work_time)

        self.config_widget.small_break_time_edit.setTime(small_break_new_time)
        self.config_widget.big_break_time_edit.setTime(big_break_new_time)
        self.config_widget.work_time_edit.setTime(work_time_new_time)

        logger.info(f"Applied new 'Time' settings: {new_small_break_time=}, {new_big_break_time=}, {new_work_time=}")
        print(f"New 'Time' settings: {new_small_break_time=}, {new_big_break_time=}, {new_work_time=}")

        # New 'Pomidoro' settings
        new_round_times = self.config_widget.round_spin_box.value()
        new_day_times = self.config_widget.day_spin_box.value()

        self.config_widget.round_spin_box.setValue(new_round_times)
        self.config_widget.day_spin_box.setValue(new_day_times)

        logger.info(f"Applied new 'Pomidoro' settings: {new_round_times=}, {new_day_times=}")
        print(f"New 'Pomidoro' settings: {new_round_times=}, {new_day_times=}")

        # New 'Notification' settings
        new_text_check_box_status = self.config_widget.text_check_box.isChecked()
        new_sound_check_box_status = self.config_widget.sound_check_box.isChecked()

        self.config_widget.text_check_box.setChecked(new_text_check_box_status)
        self.config_widget.sound_check_box.setChecked(new_sound_check_box_status)

        logger.info(f"Applied new 'Notification' settings: {new_text_check_box_status=}, {new_sound_check_box_status=}")
        print(f"New 'Notification' settings: {new_text_check_box_status=}, {new_sound_check_box_status=}")

        # New 'Basic' settings
        new_theme_color = self.config_widget.theme_combo_box.currentIndex()
        self.config_widget.theme_combo_box.setCurrentIndex(new_theme_color)

        if new_theme_color == 0:
            logger.info(f"Applied new 'Basic' settings: new_theme_color= 'Dark'")
        elif new_theme_color == 1:
            logger.info(f"Applied new 'Basic' settings: new_theme_color= 'Light'")
        print(f"New 'Basic' settings: {new_theme_color=}")
