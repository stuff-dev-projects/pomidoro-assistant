from PyQt5 import QtCore, QtWidgets

from settings import logger
from style.about_style import get_about_dialog_style


class AboutDialogUi(QtWidgets.QDialog):
    def __init__(self):
        super(AboutDialogUi, self).__init__()
        self.dialog = QtWidgets.QDialog()
        self.button_ok = QtWidgets.QDialogButtonBox(self.dialog)
        self.text_browser = QtWidgets.QTextBrowser(self.dialog)
        self.label = QtWidgets.QLabel(self.dialog)

        self.dialog.setFixedSize(330, 280)

        self.setup_ui(self.dialog)
        self.dialog.setModal(True)
        self.dialog.exec()

    def setup_ui(self, dialog_window):
        """Setup window parameters."""
        dialog_window.setObjectName("dialog")
        dialog_window.setWindowModality(QtCore.Qt.WindowModal)
        dialog_window.setStyleSheet("background-color: rgb(46, 52, 54);\n")

        self.button_ok.setGeometry(QtCore.QRect(10, 250, 311, 25))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.button_ok.sizePolicy().hasHeightForWidth())

        self.button_ok.setSizePolicy(sizePolicy)
        self.button_ok.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.button_ok.setAutoFillBackground(False)
        self.button_ok.setStandardButtons(QtWidgets.QDialogButtonBox.Ok)
        self.button_ok.setCenterButtons(True)
        self.button_ok.setObjectName("button_ok")
        self.button_ok.clicked.connect(self.button_ok_pressed)

        self.text_browser.setGeometry(QtCore.QRect(10, 50, 311, 191))
        self.text_browser.setObjectName("text_browser")

        self.label.setGeometry(QtCore.QRect(10, 10, 321, 31))
        self.label.setStyleSheet("font: 24pt \"Ubuntu\";")
        self.label.setObjectName("label")

        self.re_translate_ui(dialog_window)
        QtCore.QMetaObject.connectSlotsByName(dialog_window)

    def re_translate_ui(self, dialog):
        _translate = QtCore.QCoreApplication.translate
        dialog.setWindowTitle(_translate("AboutDialog", "About"))
        self.text_browser.setHtml(_translate("AboutDialog", get_about_dialog_style()))
        self.label.setText(_translate("AboutDialog", "𝒫𝑜𝓂𝒾𝒹𝑜𝓇𝑜 𝒜𝓈𝓈𝒾𝓈𝓉𝓂𝒶𝓃𝓉"))

    def button_ok_pressed(self):
        logger.info("'OK' button in 'About dialog' was pressed")
        self.dialog.close()
