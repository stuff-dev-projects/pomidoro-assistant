import logging


class LoggingInfo:
    """Logging information."""
    def __init__(self, service, log_level='info'):
        self.service = service
        self.logger = logging.getLogger(service)

        self.set_log_level(log_level)
        self.format = '%(levelname)s %(asctime)s: %(message)s'

    def set_log_level(self, log_level):
        if log_level == 'info':
            self.logger.setLevel(logging.INFO)
        elif log_level == 'debug':
            self.logger.setLevel(logging.DEBUG)
        elif log_level == 'critical':
            self.logger.setLevel(logging.CRITICAL)
        elif log_level == 'error':
            self.logger.setLevel(logging.ERROR)
        elif log_level == 'fatal':
            self.logger.setLevel(logging.FATAL)
        elif log_level == 'warn':
            self.logger.setLevel(logging.WARN)
        elif log_level == 'warning':
            self.logger.setLevel(logging.WARNING)

    def log_to_file(self, file_name=None):
        """Logging information to file."""
        if file_name is None:
            file_name = f'{self.service}.log'
        logger_handler = logging.FileHandler(file_name)
        logger_formatter = logging.Formatter(self.format)
        logger_handler.setFormatter(logger_formatter)
        self.logger.addHandler(logger_handler)
        self.logger.info('###_INIT_###')
        self.logger.info('Logging setup is over!')

    def info(self, msg):
        """Logging info level message."""
        self.logger.info(msg)

    def debug(self, msg):
        """Logging debug level message."""
        self.logger.debug(msg)

    def critical(self, msg):
        """Logging critical level message."""
        self.logger.critical(msg)

    def error(self, msg):
        """Logging error level message."""
        self.logger.error(msg)

    def fatal(self, msg):
        """Logging fatal level message."""
        self.logger.fatal(msg)

    def warning(self, msg):
        """Logging warning level message."""
        self.logger.warning(msg)
