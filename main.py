import sys

from PyQt5 import QtWidgets

from srs.app import AppWindowUi

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    main_window = AppWindowUi()
    sys.exit(app.exec_())
